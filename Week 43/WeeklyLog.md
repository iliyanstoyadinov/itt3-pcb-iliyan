# Week 43 log file

## Research for circuit I want to make
* I want to make an amplifier, because of the project last semester - smart alarm. I want to improve the sound of it and make it better.
* Source of the schematic: https://www.youspice.com/category/spicedocumentation/orcadpspice/orcadpcbarticles/

## Schematic in OrCAD

* Make new project
* Add all libraries
* In the schematic we have 3 amplifiers - LF411, 6 resistors that are 100K Ohm and 1 resistor 10K Ohm, 2 capacitors - 10uF. 
For the input we have 2 pin connector and the output is with 4 pin connector
* ![picture 1, OrCAD Schematic](OrCAD.PNG)

## Components that are used - datasheets

* Resistors - 1x10K Ohm and 6x100K Ohm
* Capacitors - 2x10uF
* Amplifiers - 3xLF411 - datasheet: https://www.ti.com/lit/ds/symlink/lf411.pdf