## Week 49 log file

# Prepare Poster

* The poster is in the folder as PDF file -> [Poster](https://gitlab.com/iliyanstoyadinov/itt3-pcb-iliyan/blob/master/Week%2049/PosterPCB.pdf)

# Prepare Pitch

* Pitch is in the folder as PDF file -> [Pitch](https://gitlab.com/iliyanstoyadinov/itt3-pcb-iliyan/blob/master/Week%2049/PitchPCB.pdf)