# Week 46 log file

## Routed board and outline
* The board is routed and we need to add outline.
* Click Outline -> Design
* Then you need to click on Place rectangle and the needed Height and Width.
* I am using 1600 Width and 1200 height in Mils

* ![picture 1, PCB](PCB.PNG)

## Clear DRC errors

If there are any DRC errors they should be cleaned. I didn't have any from the beggining. 

## Gerber and Drill files

* Go to Nordcad -> Output and postprocessing -> Run post process to create a zip file with both Gerber and Drill files.
* There will be promp to add Drill legend if you didn't add it before.

* Gerber view using "gerbv":

* ![picture 2,Gerber](GerberView.PNG)

## Bill of materials

* ![picture 3,bill of materials](BOM.PNG)

## PCB is ordered from JLCPCB and it will be delivered in one week. After that soldering starts.