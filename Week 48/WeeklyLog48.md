# Week 48 log file

## Assemble the PCB

* Top layer
* ![picture 1, Assembled PCB](top.jpg)

* Bottom layer
* ![picture 2, Assembled PCB](bot.jpg)

## Test the PCB if it is working

* With input of 2.5V and 5V we get 11.29V output

* ![picture 3, test PCB](test.jpg)

* If we reverse the inputs it is -11.38V

## Advanced PCB theory lecture hosted by Ilias Esmati

* Thermal Management
* Heat sinks
* Thermal interface materials (TIM)
* Fans proper PCB layout
* Reliability and safe performance of components