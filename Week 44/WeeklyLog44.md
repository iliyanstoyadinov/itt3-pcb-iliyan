# Week 44 log file

## Simulations

* All components are with PSpice profile attached.
* On the screenshot below I made input/output simulation.
* Input: Red - V1 = 4V, Green - V2 = 6V
* Output: Yellow ~ 11V
* ![picture 1, Simulation](Simulation.PNG)

* Simulation settings:
* ![picture 2, Simulation Settings](SimSettings.PNG)

## Footprints

* Resistor - R1206
* Capacitor - CAP_0805
* 2 Pin Connector - HEADER_2PINS_2_54
* 4 Pin Connector - HEADER_4PINS_2_54
* Amplifiers - I made the foorptint. Changing the DIL08 that was in the libraries to make only 7 pins for my ampifier. Screenshot:
* ![picture 3, Modified Footprint](ModFootprint.PNG)