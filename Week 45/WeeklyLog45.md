# Week 45 log file

## Netlist and component placing

* In OrCAD Capture - from Tools -> Create Netlist
* Netlist settings below:

* ![picture 1, Netlist settings](Netlist.PNG)

* After clicking "OK" and if there are no errors, PCB Editor is opened.

* From Place -> Components Manually you can see all the components that are in Capture. Click on every one of them and place it.

## Route the PCB

* After placing the components it is time to do the routing. Clicking Route -> Connect or simply "W" on the keyboard it will give you the routing tool.
* All edges must be 45 or less degrees.
* Routed board below:

* ![picture 2, Routed Board](PCB.PNG)

* Yellow is the bottom layer and green is the top layer.